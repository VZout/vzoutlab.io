---
layout: page 
title:  Monster Jam&#58; Crush It!
genre: Racing Game
platform: Windows, PS4, XBox One
publisher: Maximum Games
game-studio: Team6
engine: Unreal Engine 4
date:   2016-02-28 16:14:42 -0700
header: images/monsterjam.jpg
category: game
---

Bringing the family-friendly entertainment of live Monster Jam events straight to your tablet or mobile phone all year round, Monster Jam is an authentic and dynamic mobile gaming experience! Monster Jam will not only allow you to relive your favourite Monster Jam moments but to create your own memorable monster truck stunts using all the biggest names in Monster Jam, including Grave Digger, El Toro Loco, Maximum Destruction and Monster Mutt! Smash through barriers in actual Monster Jam events and races, or freestyle through unique environments and hill climbs!

Monster Jam: Crush It is now available for the XBox one [here](https://www.microsoft.com/en-us/store/p/monster-jam-crush-it/c1k3bjf2p9lb).

## Trailer

<iframe width="560" height="315" src="https://www.youtube.com/embed/38G3SK0XkGw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
