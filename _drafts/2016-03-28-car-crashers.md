---
layout: page 
title:  Car Crashers
genre: Racing Game
platform: PC, PS4 and Xbox One
engine: Unreal Engine
date:   2016-03-28 16:14:42 -0700
header: images/monsterjam_0.jpg
category: other_work 
---

This is a abandoned project from [Team6](http://web.team6-games.com/site/) (It may still be in development but under a different name).

The idea of this game is to race other people from a top-down perspective. But you have a massive arsenal of weapons available. This game had lots of action and explosions. Sadly I have very little footage from this game.
