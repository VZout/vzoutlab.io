---
layout: work 
title:  Planet Blitz
genre: Bullet Hell
Client: "Breda University"
platform: PC
engine: Unreal Engine
date:   2017-06-29 18:00:00 -0700
header: images/planet_blitz/header.jpg
bottom: images/planet_blitz/team_0.jpg
category: other_work
role: Programming Lead &#38; Gameplay Programmer
teamsize: 14
prodtime: 7 weeks
tech: C++, Unreal Engine
client: Breda University
website: http://planetblitz.vzout.com/
---

Planet Blitz is a top-down bullet hell game, full of high-paced action and anticipation. The player plays as a Nature Spirit, who is tasked with defending all of the inhabitants of the forest from harm as alien robot forces invade. Defending all of the diverse towns against all of the different enemy waves is key to both your own and the forest's survival.

# My Work

I implemented the physics system for the game, player movement and shooting mechanics. I also worked on the UI/UX experience.

# Trailer

<iframe width="560" height="315" src="https://www.youtube.com/embed/r2xdg-pyDYY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
