---
layout: page
title:  Global Illumination Light Mapper
platform: PC
genre: DirectX 12
engine: Custom Renderer
date:   2018-04-20
header: images/dx12_raytracer/img_1.png
bottom: images/dx12_raytracer/img_0.png
category: other_work 
---

## The Project

<span style="color:red; font-size: 18px;">
***WARNING: Images severly outdated***
</span>

This raytracer uses the same rendering code for both CPU and GPU raytracing. My plan is to be able to render part of the screen on the CPU and part on the GPU. In a situation where I have a massive amounts of data to render I would render 90% of the screen on the CPU and the reminaing 10% on the GPU for example. Since copying the data to the GPU can take quite long with complex scenes it might be efficient to do this in some rare cases I hope.

## Screenshots

![single reflection](../images/dx12_raytracer/img_0.png)
