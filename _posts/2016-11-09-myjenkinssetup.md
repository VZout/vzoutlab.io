---
layout: post
title:  Automated Builds With Jenkins
date:   2016-11-9
header: images/vim.png
thumbnail: images/jenkins-logo.png
description: A little post about my Jenkins setup experience.
category: Tools 
---

## My Setup
I could have used CMake and write some scripts to generate the builds but I also wanted a easy to use web interface. I did not have the time to write that so I decided I needed to find a build automation software. I found multiple candidates like Azure but I also wanted to create pipeline's for my personal projects that don't run on Visual Studio so I decided to use Jenkins.

Jenkins allows cross platform builds using slaves, plugins and good build triggers. It is based on Java and has a easy to use web interface. I installed a perforce plugin that allowed me to use the same workspace for multiple jobs because I did not want multiple copies of all my projects on my server and have to sync every single one separately. I also installed a plugin that allows me to build using MSBuild. MSBuild allows me to build for different architectures and configurations.

My pipeline is setup as followed: Sync perforce workspace -> Build Framework -> Build Mario (x64/x86) -> Build Mario Release (x64/x86) -> Upload Artifacts -> Clean Build Files -> Send email to the person who broke the build. When any of these steps fail the pipeline gets stopped and no artifacts will be saved. If I wanted to add unit tests or new builds I could do it very easily by changing the build parameters of MSBuild.

I first installed Jenkins on my desktop computer. It took around 4 hours to get it fully setup and working as expected. After a week of testing jenkins, fixed issues in my pipeline I decided I wanted to move Jenkins to a VPS hosted by a VPS provider.

Moving Jenkins to a VPS was not as easy as I expected. Since I am not allowed to open any ports I needed to use the only open port; 80. I quickly found out Jenkins did not recommend this because this causes a security risk and they advice to create a reverse proxy from Apache to the Jenkins address. This was quite easy due to there well explained article called [Running Jenkins behind Apache](https://wiki.jenkins-ci.org/display/JENKINS/Running+Jenkins+behind+Apache). But I was not aware at the time of IIS so I later uninstalled apache and reconfigured jenkins to work with ISS's reverse proxy later.

## Result
You can find my jenkins website at [jenkins.vzout.com](http://jenkins.vzout.com/)
