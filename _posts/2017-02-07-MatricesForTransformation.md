---
layout: post
title: Matrices For Transformation
date:   2016-11-26
header: images/vim.png
description: A introduction to matrices in graphics programming.
category: Mathmatics
---

# Rotation
Rotations can be either left or right handend. In this parapgraph all roiations will be using the left handed rule and for demonstration purposes I'll use 3x3 matrices. For real transformations we normally use 4D matrices to allow easy multiplication with different transformation matrices.

Lets start with rotation around the X axis:

$$
\begin{bmatrix} 
1 & 0 & 0 \\ 
0 & \cos(0) & \sin(0) \\
0 & -\sin(0) & \cos(0)
\end{bmatrix}
$$

The matrix to rotate around the Y axis is:

$$
\begin{bmatrix} 
\cos(0) & 0 & -\sin(0) \\ 
0 & 1 & 0 \\
\sin(0) & 0 & \cos(0)
\end{bmatrix}
$$

And last but not least. The Z axis:

$$
\begin{bmatrix} 
\cos(0) & \sin(0) & 0 \\ 
-\sin(0) & \cos(0) & 0 \\
0 & 0 & 1
\end{bmatrix}
$$

Note that these matrices work in both the left and right handed cordinate system.

We can also rotate around an arbitary axis. We do this with the following matrix with **x, y** and **z** being the axis to rotate around:

$$
\begin{bmatrix} 
x^2 \times (1 - \cos(0)) & x \times y \times (1 - \cos(0)) + z \times \sin(0) & x \times z \times (1 - \cos(0) - y \times \sin(0)) \\ 
x \times y \times (1 - \cos(0)) - z \times \sin(0) & 0 & 0 \\
0 & 0 & 1
\end{bmatrix}
$$

# Scale
We can scale matrices both uniform and nonuniform. Scaling in a non arbitrary direction can be done by multiplying the **M***11*, **M***22* and **M***33* values of a matrix by a scalar value.

# Reflection (Mirrorring)
Reflection flips or mirrors a matrix along a axis. This can be done my multiplying the scale values of a vector by -1

# Shearing (Skewing)
Shearing is a transformation that "skews" the coordinate space, stretching it nonuniformly. Angles are not preserved but the surface area will remain the same.This is quite hard to visualize so here is a image to help: 

![1. Regular square 2. Square sheared along the x-axis 3. The square seen in 2 sheared on the y-axis](http://i.imgur.com/oISrXv4.gif?1)

Shearing can be done my modifying the values **M***12*, **M***21* and **M***31*.

# Determinant
Square matrices have a special scalar called the determinant. The determinant of a $2 \times 2$ matrix is given by:

$$ m =
\begin{bmatrix} 
m12 & m12 \\ 
m21 & m22
\end{bmatrix}
=
m11 \times m22 - m12 \times m21
$$

This can be quite hard to remember especially if you are working with larger matrices. But it is actually quite easy to remember if you visualize it with some lines. 

![](../images/matrices/determinant_2x2_visualized.png){:style="max-width:100px"}

Now we will calculate the determinant of a $3 \times 3$ matrix. Again I'll start with the formula:

$$ m =
\begin{bmatrix} 
m12 & m12 & m13 \\ 
m21 & m22 & m23 \\
m31 & m32 & m33
\end{bmatrix}
=
m11 \times m22 \times m33 + m12 \times m23 \times m31 + m13 \times m21 \times m32 - m13 \times m22 \times m31 - m12 \times m21 \times m33 - m11 \times m32 \times m32
=
m11(m22 \times m33 - m23 \times m32)
+ m12(m23 \times m31 - m21 \times m33)
+ m13(m21 \times m32 - m22 \times m31)
$$

Sorry for the mess... But it will become more clear when I show you a diagram to remember the formula just like I did with the $2 \times 2$ matrix.


![](../images/matrices/determinant_3x3_visualized.png){:style="max-width:500px"}

# Inverse
Square matrices can be inverted with the following formula:

$$
M = M^-1M
$$

But not every square matrix can be inverted. An example is a matrix with a row or column filled with zero's. No matter what you multiply this matrix by the corrosponding row or column in the result will also be full of zero's.

We can use the determinant of a matrix to test for invertibility. If the determinant is zero the matrix is nonsingular(not invertable) 4and if the determinant is  the matrix is singular (invertable).

To visualize matrix invertion have a look at this image ![](../images/matrices/color_pattern.png){:style="max-width:64px"} If we would invert the model matrix of this image this would be the result: ![](../images/matrices/color_pattern_inverted.png){:style="max-width:64px"}
