---
permalink: /wisp/
layout: work
title:  Wisp Renderer
Client: "Breda University"
platform: Windows 10
genre: DirectX 12 / DirectX Raytracing
engine: Custom RTX Renderer
date:   2019-01-04
header: images/wisp/suntemple_0.jpg
bottom: images/wisp/sponza.png
category: game 
role: Tech Lead &#38; Graphics Programmer
teamsize: 8
prodtime: 22 weeks
tech: C++20, DirectX 12, DirectX Raytracing
client: Breda University
source: https://github.com/TeamWisp/WispRenderer#-wisprenderer---real-time-raytracing-renderer
---

<div class="github-card" data-github="TeamWisp/WispRenderer" data-width="400" data-height="316" data-theme="medium"></div>
<script src="https://lab.lepture.com/github-cards/widget.js"></script>

<a href="https://twitter.com/wisprenderer?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @wisprenderer</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Wisp is a general purpose high level rendering library. Specially made for real time raytracing with NVIDIA RTX graphics cards. Made by a group of students from [Breda University of Applied Sciences](https://www.buas.nl/) specializing in graphics programming.

This renderer integrates with **Autodesk Maya** to replace its viewport with a real time raytraced viewport. The plugin is available here:

<a href="https://github.com/TeamWisp/WispForMaya"><img class="github-card2" src="https://github-link-card.s3.ap-northeast-1.amazonaws.com/TeamWisp/WispForMaya.png" width="460px"></a>

## Renderer Features

* Physically Based Rendering
* Ray Traced Global Illumination
* Ray Traced Ambient Occlusion
* Ray Traced Reflections
* Ray Traced Shadows
* Translucency & Transparency.
* Depth of Field (with Bokeh)
* Post Processing (Bloom, Tonemapping, Film Grain, FXAA)
* NVIDIA's HBAO+
* NVIDIA's AnselSDK
* Autodesk Maya Integration

## Awards

We got listed by NVIDIA in their **worlds top 40 3D applications** with RTX integration!

[Check it out here](https://www.nvidia.com/en-us/design-visualization/rtx-enabled-applications/)!

## Trailer

<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/cOxVArIFmEg' frameborder='0' allowfullscreen></iframe></div>

## My Work

I wrote the **core of the renderer**, **raytracing backend**, **deferred renderer**, **HBAO+**, **Ansel integration**, **path traced pipeline**, **global illumination** and **raytracing mip level generation**. As the technology a big part of my job was to fix bugs, work on optimizations and make decisions on how features should be implemented.

As a tech lead I also made sure the code was of a good quality by doing regular code reviews and that team was working efficiently.

## Screenshots

![Depth of Field](../images/wisp/dof.jpg)
![Suntemple at night](../images/wisp/suntemple_1.png)
![Samuarai](../images/wisp/samurai.jpg)
