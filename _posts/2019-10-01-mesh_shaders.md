---
layout: work
permalink: /mesh_shading/
title:  Turing Mesh Shaders
Client: "Breda University"
platform: Windows 10, Linux
genre: Vulkan / RTX Raytracing / Mesh Shaders
engine: Research Project
date:   2019-11-02
header: images/meshshading/starwars.jpg
bottom: images/meshshading/forest.jpg
category: game
project-type: Research
role: Tech Lead &#38; Graphics Programmer
prodtime: 10 weeks
tech: C++20, Vulkan, Raytracing, Mesh Shaders
source: https://github.com/VZout/RTX-Mesh-Shaders
---

<div class="github-card" data-github="VZout/RTX-Mesh-Shaders" data-width="400" data-height="316" data-theme="medium"></div>
<script src="//cdn.jsdelivr.net/github-cards/latest/widget.js"></script>

<span style="color:red; font-size: 18px;">
***WARNING: This project is work in progress***
</span>

<p align="center">
  <strong>
    Visit <a aria-label="meshshading.vzout.com" href="http://meshshading.vzout.com/">meshshading.vzout.com</a> to read the research paper.
  </strong>
</p>

---

**This repository demo's mesh shaders introduced by NVIDIA's Turing Architecture and is used for research purposes. To accompany this project I've written a research paper on the topic of mesh shaders where I compare traditional techniques with their mesh shading equivalent.**

---

## Features

* Mesh Shading Pipeline
* Cook-Torrance Microfacet Specular BRDF
* Burley Diffuse BRDF
* Clear Coat Materials
* Anisotropic Materials
* Approximated Subsurface Scattering Materials
* Deferred Rendering
* Normal and Ambient Occlusion Mapping
* High Dynamic Range Tonemapping
* Image-Based Lighting

## Building

To build this project you require [**CMake 3.15**](https://cmake.org/) or higher and a **C++20 capable compiler**. To run the application you need a GPU and driver that **supports Vulkan 1.1**. To run the application and make use of mesh shaders you need a **[RTX capable gpu](https://en.wikipedia.org/wiki/Nvidia_RTX)**.

* [Visual Studio 2019 (16.2.3)](https://visualstudio.microsoft.com/)
* [GCC 9.2](https://gcc.gnu.org/)
* [Clang 8.0.0](https://clang.llvm.org/)

and the following operating systems are tested:

* Windows 10 (version 1903)
* Ubuntu (18.04 LTS)
* Arch Linux (Antergos 19.4)

To generate the project files run the following command in your terminal:

```sh
mkdir build
cd build
cmake -G "[configuration]" ..
```

## Screenshots


<img src="../images/meshshading/starwars.jpg"><br>
<img src="../images/meshshading/forest.jpg"><br>
<img class="small_image" src="../images/meshshading/sss.png"> 
<img class="small_image" src="../images/meshshading/ball.png"> 
